import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Main {

    // Complete the miniMaxSum function below.
    static void miniMaxSum(int[] arr) {
        long sum =0, min=0,max=0;
        for(int i=1; i<arr.length;i++){
            max+=arr[i];
            min=max;
        }
        for (int i =0; i<arr.length;i++){
            for (int j = 0 ; j<arr.length;j++){
                if(i!=j){
                   sum+=arr[j];
                }
            }
            if(sum >max){
                max=sum;
            }
            else if(sum<min){
                min=sum;
            }
            sum =0;
        }
        System.out.println(min +" "+ max);


    }


    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};

        miniMaxSum(arr);

    }
}
